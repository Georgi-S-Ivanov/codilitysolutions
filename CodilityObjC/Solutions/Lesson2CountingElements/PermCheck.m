//
//  PermCheck.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/2/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "PermCheck.h"

@implementation PermCheck

+(int)solution:(NSMutableArray *)A
{
    long int* arr = calloc(sizeof(long int) * A.count, sizeof(long int));
    
    for (int i = 0; i < A.count; i++)
    {
        long int value = [A[i] integerValue];
        
        if(value < 1 || value > A.count)
        {
            return 0;
        }
        else if(arr[value - 1] == 1)
        {
            return 0;
        }
        else
        {
            arr[value - 1] = 1;
        }
    }
    
    return 1;
}

@end
