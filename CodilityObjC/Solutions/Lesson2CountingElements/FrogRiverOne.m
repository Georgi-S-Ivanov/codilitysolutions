//
//  FrogRiverOne.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/2/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "FrogRiverOne.h"

@implementation FrogRiverOne

+(int)solutionX:(int)X A:(NSMutableArray *)A
{
    int* arr = calloc(sizeof(int) * X, sizeof(int));
    int filledPositions = 0;
    
    for (int i = 0; i < A.count; i++)
    {
        int index = (int)[A[i] integerValue];
        if(arr[index] == 0)
        {
            arr[index] = 1;
            filledPositions++;
            
            if(filledPositions == X)
            {
                return i;
            }
        }
    }
    
    return -1;
}

@end
