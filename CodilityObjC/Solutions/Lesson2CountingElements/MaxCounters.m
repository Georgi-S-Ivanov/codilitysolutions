//
//  MaxCounters.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/2/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "MaxCounters.h"

@implementation MaxCounters

+(NSMutableArray *)solutionN:(int)N A:(NSMutableArray *)A
{
    NSMutableArray* counters = [[NSMutableArray alloc] initWithCapacity:N];
    NSInteger maxCounter = 0;
    
    for (int i = 0; i < N; i++)
    {
        [counters addObject:@(0)];
    }
    
    for (int i = 0; i < A.count; i++)
    {
        NSInteger index = [A[i] integerValue] - 1;
        if(index < N)
        {
            // increase
            NSNumber* number = counters[index];
            NSInteger newValue = [number integerValue] + 1;
            [counters replaceObjectAtIndex:index
                                withObject:@(newValue)];
            if(newValue > maxCounter)
            {
                maxCounter = newValue;
            }
        }
        else
        {
            // max counter
            for (int j = 0; j < N; j++)
            {
                [counters replaceObjectAtIndex:j
                                    withObject:@(maxCounter)];
            }
        }
    }
    
    return counters;
}

@end
