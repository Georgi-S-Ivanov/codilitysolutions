//
//  MissingInteger.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/3/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "MissingInteger.h"

@implementation MissingInteger

+(int)solution:(NSMutableArray *)A
{
    int smallestIndex = 0;
    NSInteger* arr = calloc(sizeof(NSInteger) * A.count, sizeof(NSInteger));
    
    for (NSNumber* number in A)
    {
        NSInteger num = [number integerValue];
        if(num < A.count && num > 0)
        {
            if (num - 1 == smallestIndex)
            {
                for (int i = smallestIndex + 1; i < A.count; i++) {
                    if(arr[i] == 0)
                    {
                        smallestIndex = i;
                        break;
                    }
                }
            }
            else
            {
                arr[num - 1] = 1;
            }
        }
    }
    
    int result = smallestIndex + 1;
    if(A.count == 1 && arr[0] == 0 && [A[0] integerValue] != 1)
    {
        result = 1;
    }
    else if(result == A.count)
    {
        result++;
    }
    
    return result;
}

@end
