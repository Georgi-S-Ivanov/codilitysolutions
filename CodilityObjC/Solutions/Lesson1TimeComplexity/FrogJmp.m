//
//  FrogJmp.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/2/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "FrogJmp.h"

int solution(int X, int Y, int D) {
    long double distance = Y - X;
    
    long double jumps = distance / (float)D;
    jumps = ceill(jumps);
    
    return jumps;
}

@implementation FrogJmp

+(int)solutionX:(int)x Y:(int)y D:(int)d
{
    return solution(x, y, d);
}

@end
