//
//  FrogJmp.h
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/2/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SolutionProtocol.h"

@interface FrogJmp : NSObject <SolutionProtocol>

@end
