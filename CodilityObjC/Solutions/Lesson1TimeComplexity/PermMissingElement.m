//
//  PermMissingElement.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/2/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "PermMissingElement.h"

@implementation PermMissingElement

+(int)solution:(NSMutableArray *)A
{
    long double sumOfAll = ((A.count + 1) * ((long double)A.count + 2)) / 2.0f;
    
    for (NSNumber* number in A)
    {
        sumOfAll -= [number integerValue];
    }
    
    return sumOfAll;
}

@end
