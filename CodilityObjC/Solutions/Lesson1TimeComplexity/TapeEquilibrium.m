//
//  TapeEquilibrium.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/2/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "TapeEquilibrium.h"

@implementation TapeEquilibrium

+(int)solution:(NSMutableArray *)A
{
    long int leftSide = [A[0] integerValue];
    long int rightSide = 0;
    for (int i = 1; i < A.count; i++) {
        rightSide += [A[i] integerValue];
    }
    
    long int minDiff = labs(leftSide - rightSide);
    
    for (int p = 2; p < A.count; p++)
    {
        long int value = [A[p - 1] integerValue];
        leftSide += value;
        rightSide -= value;
        
        long int currentDiff = labs(leftSide - rightSide);
        if(currentDiff < minDiff)
        {
            minDiff = currentDiff;
        }
    }
    
    return (int)minDiff;
}

@end
