//
//  Solution.h
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/2/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SolutionProtocol <NSObject>

@optional

+(int)solutionX:(int)x Y:(int)y D:(int)d;
+(int)solution:(NSMutableArray*)A;
+(int)solutionX:(int)X A:(NSMutableArray*)A;
+(NSMutableArray*)solutionN:(int)N A:(NSMutableArray*)A;
+(int)solutionA:(int)A B:(int)B K:(int)K;
+(NSMutableArray*)solutionString:(NSString*)S P:(NSMutableArray*)P Q:(NSMutableArray*)Q;
+(int)solutionS:(NSString*)S;
+(int)solutionA:(NSMutableArray*)A B:(NSMutableArray*)B;

@end
