//
//  EquiLeader.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/7/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "EquiLeader.h"

@implementation EquiLeader

+(int)solution:(NSMutableArray *)A
{
    // algorithm for finding the leader (mode) in the whole sequence
    int size = 0, value = 0;
    
    for (int i = 0; i < A.count; i++)
    {
        if(size == 0)
        {
            size++;
            value = (int)[A[i] integerValue];
        }
        else
        {
            if(value != [A[i] integerValue])
            {
                size -= 1;
            }
            else
            {
                size += 1;
            }
        }
    }
    
    int candidate = -1;
    if(size > 0)
    {
        candidate = value;
    }
    
    int leader = -1, count = 0;
    for (NSNumber* number in A)
    {
        if([number integerValue] == candidate)
        {
            count++;
        }
    }
    
    if(count > A.count / 2)
    {
        leader = candidate;
    }
    else
    {
        return 0;
    }
    
    int leftSideLeaders = 0, equiLeaders = 0;
    for (int i = 0; i < A.count; i++)
    {
        if([A[i] integerValue] == leader)
        {
            leftSideLeaders++;
        }
        
        if(leftSideLeaders > ((i + 1) / 2) &&
           (count - leftSideLeaders) > ((A.count - (i + 1)) / 2))
        {
            equiLeaders++;
        }
        
    }
    
    
    return equiLeaders;
}

@end
