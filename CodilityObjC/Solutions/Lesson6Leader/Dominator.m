//
//  Dominator.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/7/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "Dominator.h"

@implementation Dominator

+(int)solution:(NSMutableArray *)A
{
    int size = 0, value = 0;
    for (int i = 0; i < A.count; i++)
    {
        if(size == 0)
        {
            size++;
            value = (int)[A[i] integerValue];
        }
        else
        {
            if(value != [A[i] integerValue])
            {
                size -= 1;
            }
            else
            {
                size += 1;
            }
        }
    }
    
    int candidate = -1;
    if(size > 0)
    {
        candidate = value;
    }
    
    int index = -1, count = 0;
    for (int i = 0; i < A.count; i++)
    {
        if([A[i] integerValue] == candidate)
        {
            count++;
            index = i;
            
        }
    }
    
    if(count > A.count / 2)
    {
        return index;
    }
    else
    {
        return -1;
    }
}

@end
