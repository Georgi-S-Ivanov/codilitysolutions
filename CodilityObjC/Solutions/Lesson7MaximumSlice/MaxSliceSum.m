//
//  MaxSliceSum.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/7/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "MaxSliceSum.h"

@implementation MaxSliceSum

+(int)solution:(NSMutableArray *)A
{
    NSInteger maxSliceEnd = [A[0] integerValue];
    NSInteger maxSlice = [A[0] integerValue];
    
    for (int i = 1; i < A.count; i++)
    {
        NSNumber* number = A[i];
        maxSliceEnd = MAX([number integerValue], maxSliceEnd + [number integerValue]);
        maxSlice = MAX(maxSlice, maxSliceEnd);
    }
    
    return (int)maxSlice;
}

// 76% - wrong solution!, no need for prefix sums
//NSInteger* arr = calloc(sizeof(NSInteger) * A.count, sizeof(NSInteger));
//
//arr[0] = [A[0] integerValue];
//NSInteger maxValue = arr[0];
//for (int i = 1; i < A.count; i++)
//{
//    NSInteger value = arr[i - 1] + [A[i] integerValue];
//    arr[i] += value;
//    if([A[i] integerValue] > maxValue)
//    {
//        maxValue = [A[i] integerValue];
//    }
//}
//
//NSInteger maxEnding = 0, maxSlice = 0, maxSum = arr[0];
//
//for (int i = 1; i < A.count; i++)
//{
//    maxEnding = MAX(0, maxEnding + arr[i] + arr[i - 1]);
//    if(maxEnding > maxSlice)
//    {
//        maxSum = MAX(arr[i], maxSum);
//        maxSlice = maxEnding;
//    }
//}
//
//maxSum = MAX(maxSum, maxValue);
//return (int)maxSum;

@end
