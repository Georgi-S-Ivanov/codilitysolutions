//
//  MaxProfit.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/7/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "MaxProfit.h"

@implementation MaxProfit

+(int)solution:(NSMutableArray *)A
{
    NSInteger maxEnding = 0, maxSlice = 0;
    
    for (int i = 1; i < A.count; i++)
    {
        maxEnding = MAX(0, maxEnding + [A[i] integerValue] - [A[i - 1] integerValue]);
        maxSlice = MAX(maxSlice, maxEnding);
    }
    
    
    return (int)maxSlice;
}

@end
