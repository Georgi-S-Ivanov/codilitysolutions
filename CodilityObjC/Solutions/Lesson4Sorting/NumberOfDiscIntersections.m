//
//  NumberOfDiscIntersections.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/5/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "NumberOfDiscIntersections.h"

@implementation NumberOfDiscIntersections

+(int)solution:(NSMutableArray *)A
{
    NSComparisonResult (^sortBlock)(id obj1, id obj2) = ^NSComparisonResult(id obj1, id obj2) {
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    
    NSMutableArray* rangeUpper = [NSMutableArray new];
    NSMutableArray* rangeLower = [NSMutableArray new];
    
    for (int i = 0; i < A.count; i++) {
        NSNumber* upper = @(i + [A[i] integerValue]);
        NSNumber* lower = @(i - [A[i] integerValue]);
        [rangeUpper addObject:upper];
        [rangeLower addObject:lower];
    }
    
    [rangeUpper sortUsingComparator:sortBlock];
    [rangeLower sortUsingComparator:sortBlock];
    
    
    int count = 0, rangeLowerIndex = 0;
    for (int rangeUpperIndex = 0; rangeUpperIndex < A.count; rangeUpperIndex++)
    {
        NSInteger upperValue = [rangeUpper[rangeUpperIndex] integerValue];
        while(rangeLowerIndex < A.count &&
              upperValue >= [rangeLower[rangeLowerIndex] integerValue])
        {
            rangeLowerIndex += 1;
        }
        
        count += rangeLowerIndex - rangeUpperIndex - 1;
        if(count > 10000000)
        {
            return -1;
        }
    }
    
    return count;
}

@end
