//
//  Distinct.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/5/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "Distinct.h"

@implementation Distinct

+(int)solution:(NSMutableArray *)A
{
    NSMutableSet* set = [[NSMutableSet alloc] init];
    
    for (NSNumber* number in A)
    {
        [set addObject:number];
    }
    
    return (int)set.count;
}

// 75 %, all performance tests fail
//NSSortDescriptor* sort = [[NSSortDescriptor alloc] initWithKey:@"integerValue" ascending:YES];
//NSArray* sorted = [A sortedArrayUsingDescriptors:@[sort]];
//
//NSInteger num = INT_MIN;
//int count = 0;
//for (NSNumber* number in sorted)
//{
//    if([number integerValue] > num)
//    {
//        count++;
//        num = [number integerValue];
//    }
//}
//
//return count;

@end
