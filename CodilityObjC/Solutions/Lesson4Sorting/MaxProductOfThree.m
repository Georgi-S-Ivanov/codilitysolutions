//
//  MaxProductOfThree.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/4/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "MaxProductOfThree.h"

@implementation MaxProductOfThree

+(int)solution:(NSMutableArray *)A
{
    NSArray* sA = [A sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
    
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    
    
    int lI = (int)sA.count - 1;
    NSInteger result1 = [sA[0] integerValue] * [sA[1] integerValue] * [sA[2] integerValue];
    NSInteger result2 = [sA[lI] integerValue] * [sA[lI - 1] integerValue] * [sA[lI - 2] integerValue];
    return MAX((int)result1, (int)result2);
    
}

// 100%
//NSArray* sA = [A sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//    if ([obj1 integerValue] > [obj2 integerValue]) {
//        return (NSComparisonResult)NSOrderedDescending;
//    }
//    
//    if ([obj1 integerValue] < [obj2 integerValue]) {
//        return (NSComparisonResult)NSOrderedAscending;
//    }
//    return (NSComparisonResult)NSOrderedSame;
//}];
//
//
//int lI = (int)sA.count - 1;
//return MAX([sA[0] integerValue] * [sA[1] integerValue] * [sA[2] integerValue],
//           [sA[lI] integerValue] * [sA[lI - 1] integerValue] * [sA[lI - 2] integerValue]);

// 22 %
//NSSortDescriptor* sortDescr = [[NSSortDescriptor alloc] initWithKey:@"integerValue" ascending:NO];
//[A sortUsingDescriptors:@[sortDescr]];
//
//NSInteger result = [A[0] integerValue] * [A[1] integerValue] * [A[2] integerValue];
//
//return (int)result;
@end
