//
//  Triangle.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/5/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "Triangle.h"

@implementation Triangle

+(int)solution:(NSMutableArray *)A
{
    if(A.count < 3)
    {
        return 0;
    }
    
    NSArray* sorted = [A sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];

    
    
    for (int i = 0; i < sorted.count - 2; i++)
    {
        if([sorted[i] integerValue] <= 0)
        {
            continue;
        }
        
        NSInteger first = [sorted[i] integerValue];
        NSInteger second = [sorted[i + 1] integerValue];
        NSInteger third = [sorted[i + 2] integerValue];
        
        if(first + second > third)
        {
            return 1;
        }
        
    }
    
    return 0;
}

@end
