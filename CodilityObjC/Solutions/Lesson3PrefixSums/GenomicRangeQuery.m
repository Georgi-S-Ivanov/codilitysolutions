//
//  GenomicRangeQuery.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/3/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "GenomicRangeQuery.h"
#import <string.h>

@implementation GenomicRangeQuery

+(NSMutableArray *)solutionString:(NSString *)S P:(NSMutableArray *)P Q:(NSMutableArray *)Q
{
    char characters[S.length];
    stpcpy(characters, [S UTF8String]);
    
    NSMutableArray* nuclMap = [[NSMutableArray alloc] initWithCapacity:4];
    for (int i = 0; i < 4; i++)
    {
        [nuclMap addObject:[[NSMutableArray alloc] initWithCapacity: S.length]];
    }
    
    NSInteger currentSum = 0;
    for (int i = 0; i < S.length; i++)
    {
        
        char c = characters[i];
        if(c == 'A')
        {
            currentSum += 1;
        }
        else if(c == 'C')
        {
            currentSum += 2;
        }
        else if(c == 'G')
        {
            currentSum += 3;
        }
        else if(c == 'T')
        {
            currentSum += 4;
        }
        
    }
    
    
    return nil;
}

@end
