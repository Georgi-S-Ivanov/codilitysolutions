//
//  CountDiv.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/3/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "CountDiv.h"

@implementation CountDiv

+(int)solutionA:(int)A B:(int)B K:(int)K
{
    int result = 0;
    if(A % K == 0)
    {
        result = (B - A) / K + 1;
    }
    else
    {
        result = (B - (A - A % K)) / K;
    }
    
    return result;
}

// naive
//NSInteger divisible = 0;
//
//for (NSInteger i = A == 0 ? 1 : A; i <= B; i++)
//{
//    if(i % K == 0)
//    {
//        divisible++;
//    }
//}
//
//return (int)divisible;

@end
