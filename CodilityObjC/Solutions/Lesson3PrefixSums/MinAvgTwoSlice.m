//
//  MinAvgTwoSlice.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/3/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "MinAvgTwoSlice.h"

@implementation MinAvgTwoSlice

+(int)solution:(NSMutableArray *)A
{
    double minAvg = ([A[0] doubleValue] + [A[1] doubleValue]) / 2.0f;
    int minIndex = 0;
    double calculatedAvg;
    
    for (int i = 0; i < A.count - 2; i++)
    {
        calculatedAvg = ([A[i] doubleValue] + [A[i + 1] doubleValue]) / 2.0f;
        
        if(calculatedAvg < minAvg)
        {
            minAvg = calculatedAvg;
            minIndex = i;
        }
        
        calculatedAvg = ([A[i] doubleValue] +
                         [A[i + 1] doubleValue] +
                         [A[i + 2] doubleValue]) / 3.0f;
        
        if(calculatedAvg < minAvg)
        {
            minAvg = calculatedAvg;
            minIndex = i;
        }
    }
    
    calculatedAvg = ([A[A.count - 1] doubleValue] + [A[A.count - 2] doubleValue]) / 2.0f;
    if(calculatedAvg < minAvg)
    {
        minAvg = calculatedAvg;
        minIndex = (int)A.count - 2;
    }
    
    return minIndex;
}

// 50% solution
//double currentSum = 0;
//double smallestAvg = INTMAX_MAX;
//int smallestAvgIndex = 0;
//
//
//for (int i = 0; i < A.count - 1; i++)
//{
//    currentSum = [A[i] integerValue];
//    
//    for (int j = i + 1; j < A.count; j++)
//    {
//        double avg = (currentSum + [A[j] doubleValue]) / (j - i + 1.0f);
//        if(avg < smallestAvg)
//        {
//            currentSum += [A[j] doubleValue];
//            smallestAvg = avg;
//            smallestAvgIndex = i;
//        }
//        else
//        {
//            break;
//        }
//    }
//}
//
//return (int)smallestAvgIndex;

@end
