//
//  GenomicRangeQuery.h
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/3/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SolutionProtocol.h"

@interface GenomicRangeQuery : NSObject <SolutionProtocol>

@end
