//
//  PassingCars.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/3/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "PassingCars.h"

@implementation PassingCars

+(int)solution:(NSMutableArray *)A
{
    NSInteger carsEast = 0, passes = 0;
    
    for (NSNumber* number in A)
    {
        if([number integerValue] == 0)
        {
            carsEast++;
        }
        else
        {
            passes += carsEast;
        }
        
        if(passes > 1000000000)
        {
            passes = -1;
            break;
        }
    }
    
    return (int)passes;
}

@end
