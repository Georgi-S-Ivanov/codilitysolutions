//
//  CountDistinctSlices.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/8/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "CountDistinctSlices.h"

@implementation CountDistinctSlices

+(int)solutionX:(int)M A:(NSMutableArray *)A
{
    if(A.count == 1)
    {
        return 1;
    }
    
    int* accessed = malloc((M + 1) * sizeof(int));
    for (int i = 0; i <= M; i++)
    {
        accessed[i] = -1;
    }
    
    int front = 0, back = 0, result = 0;
    
    for (; front < A.count; front++)
    {
        NSInteger indexFront = [A[front] integerValue];
        if(accessed[indexFront] == -1)
        {
            accessed[indexFront] = front;
        }
        else
        {
            int newBack = accessed[indexFront] + 1;
            result += (newBack - back) * (front - back + front - newBack + 1) / 2;
            if(result >= 1000000000)
            {
                return 1000000000;
            }
            
            for (int i = back; i < newBack; i++)
            {
                accessed[[A[i] integerValue]] = -1;
            }
            
            accessed[indexFront] = front;
            back = newBack;
        }
    }
    front--;
    
    result += (front - back + 1) * (front - back + 2) / 2;
    
    return MIN(result, 1000000000);
}

// 70%, it fails when A.count and M are big
//NSMutableSet* stack = [[NSMutableSet alloc] init];
//int count = 0;
//
//for (int i = 0; i < A.count; i++)
//{
//    for (int j = i; j <= M + i && j < A.count; j++)
//    {
//        NSNumber* start = A[j];
//        if ([stack containsObject:start])
//        {
//            break;
//        }
//        else
//        {
//            [stack addObject:start];
//            count++;
//        }
//        
//        if(count > 1000000000)
//        {
//            return 1000000000;
//        }
//    }
//    [stack removeAllObjects];
//    
//}
//
//return count;

@end
