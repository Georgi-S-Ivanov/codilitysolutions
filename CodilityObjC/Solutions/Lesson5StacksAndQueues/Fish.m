//
//  Fish.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/7/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "Fish.h"

@implementation Fish

+(int)solutionA:(NSMutableArray *)A B:(NSMutableArray *)B
{
    NSMutableArray* stack = [[NSMutableArray alloc] initWithCapacity:A.count];
    int count = 0;
    
    for (int i = 0; i < B.count; i++)
    {
        NSInteger dir = [B[i] integerValue];
        if(dir == 0)
        {
            count++;
            
            for (NSInteger p = stack.count - 1; p >= 0; p--)
            {
                NSInteger size = [A[[stack[p] integerValue]] integerValue];

                if([A[i] integerValue] > size)
                {
                    [stack removeObjectAtIndex:p];
                }
                else
                {
                    count--;
                    break;
                }
            }
        }
        else if(dir == 1)
        {
            [stack addObject:@(i)];
        }
    }
    
    count += stack.count;
    
    return count;
}

@end
