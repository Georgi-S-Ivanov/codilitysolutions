//
//  Nesting.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/8/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "Nesting.h"

@implementation Nesting

+(int)solutionS:(NSString *)S
{
    char characters[S.length];
    stpcpy(characters, [S UTF8String]);
    
    char openP = '(', closedP = ')';
    
    if(S.length == 0)
    {
        return 1;
    }
    else if(characters[0] == closedP)
    {
        return 0;
    }
    
    int count = 0;
    for (int i = 0; i < S.length; i++)
    {
        char c = characters[i];
        if(c == openP)
        {
            count++;
        }
        else
        {
            count--;
            if(count < 0)
            {
                return 0;
            }
        }
    }
    
    return count == 0 ? 1 : 0;
}

@end
