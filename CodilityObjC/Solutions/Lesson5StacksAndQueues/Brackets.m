//
//  Brackets.m
//
//
//  Created by Georgi Ivanov on 6/6/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "Brackets.h"

@implementation Brackets

+(int)solutionS:(NSString *)S
{
    // convert nsstring to char array
    char characters[S.length];
    stpcpy(characters, [S UTF8String]);
    
    char openP = '(', openB = '[', openC = '{', closedP = ')', closedB = ']', closedC = '}';
    
    if(S.length == 0)
    {
        return 1;
    }
    else if(characters[0] == closedP || characters[0] == closedB || characters[0] == closedC)
    {
        return 0;
    }
    
    NSMutableArray* stack = [[NSMutableArray alloc] initWithCapacity:S.length];
    NSNumber* number = nil;
    
    for (int i = 0; i < S.length; i++)
    {
        char c = characters[i];
        if(c == openP || c == openB || c == openC)
        {
            [stack addObject:@(c)];
            number = nil;
        }
        else if(c == closedP)
        {
            number = @(openP);
        }
        else if(c == closedB)
        {
            number = @(openB);
        }
        else if(c == closedC)
        {
            number = @(openC);
        }
        
        if(number != nil)
        {
            if([number integerValue] == [[stack lastObject] integerValue])
            {
                [stack removeLastObject];
            }
            else
            {
                return 0;
            }
        }
    }
    
    
    return stack.count == 0 ? 1 : 0;
}

@end
