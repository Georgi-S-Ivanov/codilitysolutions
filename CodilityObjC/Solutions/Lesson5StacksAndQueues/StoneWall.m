//
//  StoneWall.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/6/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "StoneWall.h"

@implementation StoneWall

+(int)solution:(NSMutableArray*)H
{
    NSMutableArray* stack = [[NSMutableArray alloc] initWithCapacity:H.count];
    int count = 0;
    
    for (NSNumber* height in H)
    {
        while(stack.count != 0 && [height integerValue] < [stack.lastObject integerValue])
        {
            [stack removeLastObject];
            count++;
        }
        
        if(stack.count == 0 || [height integerValue] > [stack.lastObject integerValue])
        {
            [stack addObject:height];
        }
    }
    
    count += stack.count;
    
    return count;
}

// 50%
//NSMutableArray* stack = [[NSMutableArray alloc] initWithCapacity:A.count];
//int count = 0;
//
//for (NSNumber* number in A)
//{
//    NSNumber* last = stack.lastObject;
//    if([number integerValue] < [last integerValue])
//    {
//        [stack removeLastObject];
//        last = stack.lastObject;
//    }
//    
//    if([number integerValue] < [last integerValue])
//    {
//        count++;
//    }
//    else if([number integerValue] > [last integerValue])
//    {
//        [stack addObject:number];
//        count++;
//    }
//}
//
//return count;

@end
