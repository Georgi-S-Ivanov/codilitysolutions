//
//  TurtlePath.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/8/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "TurtlePath.h"

@implementation TurtlePath

// not a good solution

+(int)solution:(NSMutableArray *)A
{
    int left = 0, right = 0, top = 0, bottom = 0;
    int posX = 0, posY = 0;
    
    for (int i = 0; i < A.count; i++)
    {
        int length = (int)[A[i] integerValue];
        int dir = i % 4;
        
        if(dir == 0) // up
        {
            posY += length;
        }
        else if(dir == 1) // right
        {
            posX += length;
        }
        else if(dir == 2) // down
        {
            posY -= length;
        }
        else if(dir == 3) // left
        {
            posX -= length;
        }
        
        if((dir == 0 || dir == 2) &&
           posX > left && posX < right && i > 2)
        {
            return i;
        }
        else if((dir == 1 || dir == 3) &&
                posY > bottom && posY < top && i > 2)
        {
            return i;
        }
        
        if(posY > top)
        {
            top = posY;
        }
        if(posY < bottom)
        {
            bottom = posY;
        }
        
        if(posX > right)
        {
            right = posX;
        }
        if(posX < left)
        {
            left = posX;
        }
        
    }
    
    return 0;
}

@end
