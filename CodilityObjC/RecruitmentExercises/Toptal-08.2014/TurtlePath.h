//
//  TurtlePath.h
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/8/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SolutionProtocol.h"

@interface TurtlePath : NSObject <SolutionProtocol>

@end

//a turtle is traveling X steps(A[i]), on each turn it rotates clockwise
//first index i where it crosses its path
//
//
//assumptions:
//0 <= N <= 100000
//each element of array A is an integer within the range [1..2,147,483,647].