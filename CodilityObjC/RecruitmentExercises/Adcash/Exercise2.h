//
//  Exercise1.h
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/8/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SolutionProtocol.h"

@interface Exercise2 : NSObject <SolutionProtocol>

@end
