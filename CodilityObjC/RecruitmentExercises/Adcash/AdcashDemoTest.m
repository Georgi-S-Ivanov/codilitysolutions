//
//  AdcashDemoTest.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/8/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import "AdcashDemoTest.h"

@implementation AdcashDemoTest

+(int)solution:(NSMutableArray *)A
{
    if(A.count == 0)
    {
        return -1;
    }
    
    NSInteger* prefixSums = calloc(sizeof(NSInteger) * A.count, sizeof(NSInteger));
    
    prefixSums[0] = [A[0] integerValue];
    for (int i = 1; i < A.count; i++)
    {
        NSInteger value = prefixSums[i - 1] + [A[i] integerValue];
        prefixSums[i] = value;
    }
    
    for (int i = 0; i < A.count; i++)
    {
        NSInteger leftSideSum = i - 1 >= 0 ? prefixSums[i - 1] : 0;
        NSInteger rightSideSum = prefixSums[A.count - 1] - prefixSums[i];
        
        if(leftSideSum == rightSideSum)
        {
            return i;
        }
    }
    
    return -1;
}

@end
