//
//  Toptal_08_2014.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/8/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "TurtlePath.h"

@interface Toptal_08_2014 : XCTestCase

@end

@implementation Toptal_08_2014

//- (void)testExample
//{
//    NSMutableArray* arr = [@[@(1), @(3), @(2), @(5), @(4), @(4), @(6), @(3), @(2)] mutableCopy];
//    int sol = [TurtlePath solution: arr];
//    XCTAssertEqual(6, sol);
//}
//
//- (void)testExample1
//{
//    NSMutableArray* arr = [@[@(1), @(3), @(2), @(1), @(4), @(4), @(6), @(3), @(2)] mutableCopy];
//    int sol = [TurtlePath solution: arr];
//    XCTAssertEqual(4, sol);
//}
//
//- (void)testExample2
//{
//    NSMutableArray* arr = [@[@(1), @(3), @(3), @(5), @(1), @(6), @(6), @(3), @(2)] mutableCopy];
//    int sol = [TurtlePath solution: arr];
//    XCTAssertEqual(5, sol);
//}
//
//- (void)testExample3
//{
//    NSMutableArray* arr = [@[@(1), @(1), @(2), @(2), @(4), @(3), @(1), @(4), @(2)] mutableCopy];
//    int sol = [TurtlePath solution: arr];
//    XCTAssertEqual(7, sol);
//}
//
//- (void)testExample4
//{
//    NSMutableArray* arr = [@[@(1), @(2), @(2), @(1), @(3), @(3), @(1), @(4), @(2)] mutableCopy];
//    int sol = [TurtlePath solution: arr];
//    XCTAssertEqual(4, sol);
//}
//
//- (void)testExample5
//{
//    // this test fails
//    NSMutableArray* arr = [@[@(1), @(1), @(2), @(2), @(4), @(1), @(1), @(2), @(2)] mutableCopy];
//    int sol = [TurtlePath solution: arr];
//    XCTAssertEqual(7, sol);
//}
@end
