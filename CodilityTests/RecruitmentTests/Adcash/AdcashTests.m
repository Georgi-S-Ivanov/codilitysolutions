//
//  AdcashTests.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/8/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "Exercise1.h"
#import "Exercise2.h"
#import "AdcashDemoTest.h"

@interface AdcashTests : XCTestCase

@end

@implementation AdcashTests

//- (void)testDemoTest
//{
//    NSMutableArray* arr = [@[@(-1), @(3), @(-4), @(5), @(1), @(-6), @(2), @(1)] mutableCopy];
//    int sol = [AdcashDemoTest solution: arr];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testDemoTest1
//{
//    NSMutableArray* arr = [@[] mutableCopy];
//    int sol = [AdcashDemoTest solution: arr];
//    XCTAssertEqual(-1, sol);
//}
//
//- (void)testDemoTest2
//{
//    NSMutableArray* arr = [@[@(-1), @(1), @(2)] mutableCopy];
//    int sol = [AdcashDemoTest solution: arr];
//    XCTAssertEqual(2, sol);
//}
//
//- (void)testDemoTest3
//{
//    NSMutableArray* arr = [@[@(1), @(-3), @(3)] mutableCopy];
//    int sol = [AdcashDemoTest solution: arr];
//    XCTAssertEqual(0, sol);
//}

@end
