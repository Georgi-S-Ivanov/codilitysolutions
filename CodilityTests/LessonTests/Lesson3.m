//
//  Lesson3.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/3/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "PassingCars.h"
#import "CountDiv.h"
#import "MinAvgTwoSlice.h"
#import "GenomicRangeQuery.h"

@interface Lesson3 : XCTestCase

@end

@implementation Lesson3

//- (void)testPassingCars
//{
//    NSMutableArray* arr = [@[@(0), @(1), @(0), @(1), @(1)] mutableCopy];
//    int sol = [PassingCars solution: arr];
//    XCTAssertEqual(5, sol);
//}
//
//- (void)testPassingCars1
//{
//    NSMutableArray* arr = [@[@(1), @(1), @(0), @(1), @(1)] mutableCopy];
//    int sol = [PassingCars solution: arr];
//    XCTAssertEqual(2, sol);
//}
//
//- (void)testPassingCars2
//{
//    NSMutableArray* arr = [@[@(1), @(1), @(1), @(1), @(1)] mutableCopy];
//    int sol = [PassingCars solution: arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testPassingCars3
//{
//    int count = 100000;
//    NSMutableArray* arr = [NSMutableArray new];
//    
//    for (int i = 0; i < count; i++)
//    {
//        [arr addObject:@(0)];
//    }
//    
//    [arr replaceObjectAtIndex:99999 withObject:@(1)];
//    [arr replaceObjectAtIndex:99998 withObject:@(1)];
//    
//    int sol = [PassingCars solution: arr];
//    XCTAssertEqual(199996, sol);
//}
//
//- (void)testPassingCars4
//{
//    int count = 100000;
//    NSMutableArray* arr = [NSMutableArray new];
//    
//    for (int i = 0; i < count / 2; i++)
//    {
//        [arr addObject:@(0)];
//    }
//    
//    for (int i = count / 2; i < count; i++)
//    {
//        [arr addObject:@(1)];
//    }
//    
//    int sol = [PassingCars solution: arr];
//    XCTAssertEqual(-1, sol);
//}
//
//- (void)testCountDiv
//{
//    int A = 6;
//    int B = 11;
//    int K = 2;
//    
//    int sol = [CountDiv solutionA:A B:B K:K];
//    XCTAssertEqual(3, sol);
//}
//
//- (void)testCountDiv1
//{
//    int A = 0;
//    int B = 10;
//    int K = 2;
//    
//    int sol = [CountDiv solutionA:A B:B K:K];
//    XCTAssertEqual(6, sol);
//}
//
//- (void)testCountDiv2
//{
//    int A = 0;
//    int B = 1;
//    int K = 1;
//    
//    int sol = [CountDiv solutionA:A B:B K:K];
//    XCTAssertEqual(2, sol);
//}
//
//- (void)testCountDiv3
//{
//    int A = 0;
//    int B = 1;
//    int K = 2;
//    
//    int sol = [CountDiv solutionA:A B:B K:K];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testCountDiv4
//{
//    int A = 11;
//    int B = 345;
//    int K = 17;
//    
//    int sol = [CountDiv solutionA:A B:B K:K];
//    XCTAssertEqual(20, sol);
//}
//
//- (void)testCountDiv5
//{
//    int A = 0;
//    int B = 2000000000;
//    int K = 1;
//    
//    int sol = [CountDiv solutionA:A B:B K:K];
//    XCTAssertEqual(2000000001, sol);
//}
//
//-(void)testMinAvgTwoSlice
//{
//    NSMutableArray* arr = [@[@(4), @(2), @(2), @(5), @(1), @(5), @(8)] mutableCopy];
//    int sol = [MinAvgTwoSlice solution: arr];
//    XCTAssertEqual(1, sol);
//}
//
//-(void)testMinAvgTwoSlice1
//{
//    NSMutableArray* arr = [@[@(-4), @(2), @(2), @(5), @(1), @(-5), @(8)] mutableCopy];
//    int sol = [MinAvgTwoSlice solution: arr];
//    XCTAssertEqual(4, sol);
//}
//
//-(void)testMinAvgTwoSlice2
//{
//    NSMutableArray* arr = [@[@(5), @(1), @(-1), @(-2), @(-3), @(20)] mutableCopy];
//    int sol = [MinAvgTwoSlice solution: arr];
//    XCTAssertEqual(3, sol);
//}
//
//-(void)testMinAvgTwoSlice3
//{
//    NSMutableArray* arr = [@[@(5), @(1), @(-1), @(-2), @(-1), @(20)] mutableCopy];
//    int sol = [MinAvgTwoSlice solution: arr];
//    XCTAssertEqual(2, sol);
//}
//
//-(void)testMinAvgTwoSlice4
//{
//    NSMutableArray* arr = [@[@(5), @(4),  @(-5), @(-10),  @(-12), @(10), @(-14), @(9), @(1), @(16)] mutableCopy];
//    int sol = [MinAvgTwoSlice solution: arr];
//    XCTAssertEqual(3, sol);
//}
//
//-(void)testMinAvgTwoSlice5
//{
//    NSMutableArray* arr = [@[@(100), @(800),  @(600), @(150),  @(160), @(150), @(400), @(500)] mutableCopy];
//    int sol = [MinAvgTwoSlice solution: arr];
//    XCTAssertEqual(3, sol);
//}

//-(void)testGenomicRangeQuery
//{
//    NSString* S = @"CAGCCTA";
//    NSMutableArray* P = [@[@(2), @(5),  @(0)] mutableCopy];
//    NSMutableArray* Q = [@[@(4), @(5),  @(6)] mutableCopy];
//    
//    NSMutableArray* result = [GenomicRangeQuery solutionString:S P:P Q:Q];
//    XCTAssertEqual(2, [result[0] integerValue]);
//    XCTAssertEqual(4, [result[1] integerValue]);
//    XCTAssertEqual(1, [result[2] integerValue]);
//}

@end
