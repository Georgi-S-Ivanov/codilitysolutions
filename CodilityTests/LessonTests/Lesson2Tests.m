//
//  Lesson2Tests.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/2/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>

#import "PermCheck.h"
#import "FrogRiverOne.h"
#import "MaxCounters.h"
#import "MissingInteger.h"

@interface Lesson2Tests : XCTestCase

@end

@implementation Lesson2Tests

//- (void)testPermCheck
//{
//    NSMutableArray* arr = [@[@(4), @(1), @(3), @(2)] mutableCopy];
//    int sol = [PermCheck solution: arr];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testPermCheck1
//{
//    int count = 100000;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    
//    for (int i = 1; i < count + 1; i++)
//    {
//        [arr addObject:@(i)];
//    }
//    
//    int sol = [PermCheck solution: arr];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testPermCheck2
//{
//    int count = 100000;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    
//    for (int i = 1; i < count + 1; i++)
//    {
//        [arr addObject:@(i)];
//    }
//    
//    [arr removeObjectAtIndex:count/2];
//    
//    int sol = [PermCheck solution: arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testPermCheck3
//{
//    int count = 3333;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    
//    for (int i = 1; i < count + 1; i++)
//    {
//        [arr addObject:@(i)];
//    }
//    
//    [arr removeObjectAtIndex:0];
//    
//    int sol = [PermCheck solution: arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testPermCheck4
//{
//    int count = 10;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    
//    for (int i = count + 1; i >= 1; i--)
//    {
//        [arr addObject:@(i)];
//    }
//    
//    [arr removeObjectAtIndex:count - 2];
//    
//    int sol = [PermCheck solution: arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testPermCheck5
//{
//    NSMutableArray* arr = [@[@(1000000000)] mutableCopy];
//    int sol = [PermCheck solution: arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testPermCheck6
//{
//    NSMutableArray* arr = [@[@(1)] mutableCopy];
//    int sol = [PermCheck solution: arr];
//    XCTAssertEqual(1, sol);
//}
//
//-(void)testFrogReverOne
//{
//    NSMutableArray* arr = [@[@(1), @(3), @(1), @(4), @(2), @(3), @(5), @(4)] mutableCopy];
//    int X = 5;
//    
//    int sol = [FrogRiverOne solutionX:X A:arr];
//    XCTAssertEqual(6, sol);
//}
//
//-(void)testFrogReverOne1
//{
//    NSMutableArray* arr = [@[@(1), @(3), @(1), @(3), @(2), @(3), @(5), @(4)] mutableCopy];
//    int X = 5;
//    
//    int sol = [FrogRiverOne solutionX:X A:arr];
//    XCTAssertEqual(7, sol);
//}
//
//-(void)testFrogReverOne2
//{
//    int count = 100000;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    int X = count;
//    
//    for (int i = 0; i < count; i++)
//    {
//        [arr addObject:@(i + 1)];
//    }
//    
//    int sol = [FrogRiverOne solutionX:X A:arr];
//    XCTAssertEqual(count - 1, sol);
//}
//
//-(void)testFrogReverOne3
//{
//    NSMutableArray* arr = [@[@(1), @(2), @(1), @(3), @(2), @(3), @(1), @(3), @(2), @(3), @(1), @(3), @(2), @(3)] mutableCopy];
//    int X = 4;
//    
//    int sol = [FrogRiverOne solutionX:X A:arr];
//    XCTAssertEqual(-1, sol);
//}
//
//-(void)testFrogReverOne4
//{
//    NSMutableArray* arr = [@[@(1)] mutableCopy];
//    int X = 1;
//    
//    int sol = [FrogRiverOne solutionX:X A:arr];
//    XCTAssertEqual(0, sol);
//}
//
//-(void)testMaxCounters
//{
//    NSMutableArray* arr = [@[@(3), @(4), @(4), @(6), @(1), @(4), @(4)] mutableCopy];
//    int N = 5;
//    
//    NSMutableArray* result = [MaxCounters solutionN:N A:arr];
//    XCTAssertEqual(3, [result[0] integerValue]);
//    XCTAssertEqual(2, [result[1] integerValue]);
//    XCTAssertEqual(2, [result[2] integerValue]);
//    XCTAssertEqual(4, [result[3] integerValue]);
//    XCTAssertEqual(2, [result[4] integerValue]);
//}
//
//-(void)testMaxCounters1
//{
//    NSInteger count = 100000;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    int N = 100000;
//    
//    for (int i = 0; i < count + 1; i++)
//    {
//        [arr addObject:@(i + 1)];
//    }
//    
//    [arr replaceObjectAtIndex:count - 1 withObject:@(5)];
//    
//    NSMutableArray* result = [MaxCounters solutionN:N A:arr];
//    
//    for (int i = 0; i < count; i++)
//    {
//        XCTAssertEqual(2, [result[i] integerValue]);
//    }
//}
//
//-(void)testMaxCounters2
//{
//    NSInteger count = 100000;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    int N = 100000;
//    
//    for (int i = 0; i < count + 1; i++)
//    {
//        [arr addObject:@(i + 1)];
//    }
//    
//    
//    NSMutableArray* result = [MaxCounters solutionN:N A:arr];
//    
//    for (int i = 0; i < count; i++)
//    {
//        XCTAssertEqual(1, [result[i] integerValue]);
//    }
//}
//
//-(void)testMaxCounters3
//{
//    NSInteger count = 10000;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    int N = 10000;
//    
//    for (int i = 0; i < count + 1; i++)
//    {
//        [arr addObject:@(N + 1)];
//    }
//    
//    
//    NSMutableArray* result = [MaxCounters solutionN:N A:arr];
//    
//    for (int i = 0; i < count; i++)
//    {
//        XCTAssertEqual(0, [result[i] integerValue]);
//    }
//}
//
//-(void)testMaxCounters4
//{
//    NSMutableArray* arr = [@[@(1), @(1), @(6), @(2), @(2), @(2), @(3)] mutableCopy];
//    int N = 5;
//    
//    NSMutableArray* result = [MaxCounters solutionN:N A:arr];
//    XCTAssertEqual(2, [result[0] integerValue]);
//    XCTAssertEqual(5, [result[1] integerValue]);
//    XCTAssertEqual(3, [result[2] integerValue]);
//    XCTAssertEqual(2, [result[3] integerValue]);
//    XCTAssertEqual(2, [result[4] integerValue]);
//}
//
//-(void)testMaxCounters5
//{
//    NSMutableArray* arr = [@[@(3), @(3), @(3), @(2), @(2), @(2), @(2)] mutableCopy];
//    int N = 3;
//    
//    NSMutableArray* result = [MaxCounters solutionN:N A:arr];
//    XCTAssertEqual(0, [result[0] integerValue]);
//    XCTAssertEqual(4, [result[1] integerValue]);
//    XCTAssertEqual(3, [result[2] integerValue]);
//}
//
//-(void)testMaxCounters6
//{
//    NSMutableArray* arr = [@[@(3), @(3), @(4), @(2), @(2), @(4), @(2)] mutableCopy];
//    int N = 3;
//    
//    NSMutableArray* result = [MaxCounters solutionN:N A:arr];
//    XCTAssertEqual(4, [result[0] integerValue]);
//    XCTAssertEqual(5, [result[1] integerValue]);
//    XCTAssertEqual(4, [result[2] integerValue]);
//}
//
//-(void)testMissingInteger
//{
//    NSMutableArray* arr = [@[@(1), @(3), @(6), @(4), @(1), @(2)] mutableCopy];
//    int sol = [MissingInteger solution:arr];
//    XCTAssertEqual(5, sol);
//}
//
//-(void)testMissingInteger1
//{
//    NSMutableArray* arr = [@[@(1), @(2), @(3), @(5), @(6), @(7)] mutableCopy];
//    int sol = [MissingInteger solution:arr];
//    XCTAssertEqual(4, sol);
//}
//
//-(void)testMissingInteger2
//{
//    NSMutableArray* arr = [@[@(2147483641), @(2147483642), @(2147483643), @(2147483644), @(2147483645), @(2147483647), @(1)] mutableCopy];
//    int sol = [MissingInteger solution:arr];
//    XCTAssertEqual(2, sol);
//}
//
//-(void)testMissingInteger3
//{
//    NSMutableArray* arr = [@[@(-2147483641), @(-2147483642), @(-2147483643), @(2147483644), @(2147483645), @(2147483647), @(2), @(3)] mutableCopy];
//    int sol = [MissingInteger solution:arr];
//    XCTAssertEqual(1, sol);
//}
//
//-(void)testMissingInteger4
//{
//    NSMutableArray* arr = [@[@(1)] mutableCopy];
//    int sol = [MissingInteger solution:arr];
//    XCTAssertEqual(2, sol);
//}
//
//-(void)testMissingInteger5
//{
//    int count = 100000;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    
//    for (int i = 0; i < count; i++)
//    {
//        [arr addObject:@(i)];
//    }
//    
//    int sol = [MissingInteger solution:arr];
//    XCTAssertEqual(100001, sol);
//}
//
//-(void)testMissingInteger6
//{
//    NSMutableArray* arr = [@[@(2147483647)] mutableCopy];
//    int sol = [MissingInteger solution:arr];
//    XCTAssertEqual(1, sol);
//}

@end
