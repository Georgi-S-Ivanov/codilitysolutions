//
//  Lesson5StacksNQueues.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/6/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "Brackets.h"
#import "StoneWall.h"
#import "Fish.h"
#import "Nesting.h"

@interface Lesson5StacksNQueues : XCTestCase

@end

@implementation Lesson5StacksNQueues

//- (void)testBrackets
//{
//    NSString* s = @"{[()()]}";
//    int sol = [Brackets solutionS:s];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testBrackets1
//{
//    NSString* s = @"([)()]";
//    int sol = [Brackets solutionS:s];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testBrackets2
//{
//    NSString* s = @"";
//    int sol = [Brackets solutionS:s];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testBrackets3
//{
//    NSString* s = @"}[()()]{";
//    int sol = [Brackets solutionS:s];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testBrackets4
//{
//    NSString* s = @"()[]{[([()]){}]}";
//    int sol = [Brackets solutionS:s];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testStoneWall
//{
//    NSMutableArray* arr = [@[@(8), @(8), @(5), @(7), @(9), @(8), @(7), @(4), @(8)] mutableCopy];
//    int sol = [StoneWall solution:arr];
//    XCTAssertEqual(7, sol);
//}
//
//- (void)testStoneWall1
//{
//    NSMutableArray* arr = [@[@(1), @(2), @(3), @(2), @(1),] mutableCopy];
//    int sol = [StoneWall solution:arr];
//    XCTAssertEqual(3, sol);
//}
//
//- (void)testStoneWall2
//{
//    NSMutableArray* arr = [@[@(1000000000)] mutableCopy];
//    int sol = [StoneWall solution:arr];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testStoneWall3
//{
//    NSMutableArray* arr = [@[@(1), @(2), @(3), @(2), @(1), @(5), @(5), @(3)] mutableCopy];
//    int sol = [StoneWall solution:arr];
//    XCTAssertEqual(5, sol);
//}
//
//- (void)testStoneWall4
//{
//    NSMutableArray* arr = [@[@(1), @(2), @(3), @(4), @(5), @(6), @(7), @(8)] mutableCopy];
//    int sol = [StoneWall solution:arr];
//    XCTAssertEqual(8, sol);
//}
//
//- (void)testStoneWall5
//{
//    NSMutableArray* arr = [@[@(1), @(2), @(3), @(4), @(5), @(6), @(7), @(8)] mutableCopy];
//    int sol = [StoneWall solution:arr];
//    XCTAssertEqual(8, sol);
//}
//
//- (void)testFish
//{
//    NSMutableArray* size = [@[@(4), @(3), @(2), @(1), @(5)] mutableCopy];
//    NSMutableArray* direction = [@[@(0), @(1), @(0), @(0), @(0)] mutableCopy];
//    int sol = [Fish solutionA:size B:direction];
//    XCTAssertEqual(2, sol);
//}
//
//- (void)testFish1
//{
//    NSMutableArray* size =      [@[@(4), @(3), @(0), @(1), @(5)] mutableCopy];
//    NSMutableArray* direction = [@[@(1), @(1), @(1), @(0), @(0)] mutableCopy];
//    int sol = [Fish solutionA:size B:direction];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testFish2
//{
//    NSMutableArray* size =      [@[@(1000000000)] mutableCopy];
//    NSMutableArray* direction = [@[@(1)] mutableCopy];
//    int sol = [Fish solutionA:size B:direction];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testFish3
//{
//    NSMutableArray* size = [@[@(0), @(3), @(10000), @(1), @(5)] mutableCopy];
//    NSMutableArray* direction = [@[@(0), @(0), @(1), @(1), @(1)] mutableCopy];
//    int sol = [Fish solutionA:size B:direction];
//    XCTAssertEqual(5, sol);
//}
//
//- (void)testFish4
//{
//    NSMutableArray* size =      [@[@(4), @(1), @(2), @(3), @(5)] mutableCopy];
//    NSMutableArray* direction = [@[@(0), @(0), @(1), @(0), @(0)] mutableCopy];
//    int sol = [Fish solutionA:size B:direction];
//    XCTAssertEqual(4, sol);
//}
//
//- (void)testFish5
//{
//    NSMutableArray* size =      [@[@(8), @(3), @(2), @(1), @(4), @(5), @(9), @(7)] mutableCopy];
//    NSMutableArray* direction = [@[@(1), @(0), @(1), @(0), @(0), @(0), @(0), @(1)] mutableCopy];
//    int sol = [Fish solutionA:size B:direction];
//    XCTAssertEqual(2, sol);
//}

- (void)testNesting
{
    NSString* s = @"(()(())())";
    int sol = [Nesting solutionS:s];
    XCTAssertEqual(1, sol);
}

- (void)testNesting1
{
    NSString* s = @"(()(()())";
    int sol = [Nesting solutionS:s];
    XCTAssertEqual(0, sol);
}

- (void)testNesting2
{
    NSString* s = @")(()(())())";
    int sol = [Nesting solutionS:s];
    XCTAssertEqual(0, sol);
}

- (void)testNesting3
{
    NSString* s = @"";
    int sol = [Nesting solutionS:s];
    XCTAssertEqual(1, sol);
}

- (void)testNesting4
{
    NSString* s = @"(";
    int sol = [Nesting solutionS:s];
    XCTAssertEqual(0, sol);
}

- (void)testNesting5
{
    NSString* s = @"()";
    int sol = [Nesting solutionS:s];
    XCTAssertEqual(1, sol);
}
@end
