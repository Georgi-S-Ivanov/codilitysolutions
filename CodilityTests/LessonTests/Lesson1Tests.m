//
//  CodilityTests.m
//  CodilityTests
//
//  Created by Georgi Ivanov on 6/2/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "FrogJmp.h"
#import "PermMissingElement.h"
#import "TapeEquilibrium.h"

@interface Lesson1Tests : XCTestCase

@end

@implementation Lesson1Tests

//-(void)testFrogJmp
//{
//    int sol = [FrogJmp solutionX:500000 Y:1000000000 D:3];
//    XCTAssertEqual(333166667, sol);
//}
//
//-(void)testFrogJmp1
//{
//    int sol = [FrogJmp solutionX:0 Y:1000000000 D:7];
//    XCTAssertEqual(142857143, sol);
//}
//
//-(void)testFrogJmp2
//{
//    int sol = [FrogJmp solutionX:450000 Y:500000 D:5];
//    XCTAssertEqual(10000, sol);
//}
//
//-(void)testPermMissingElement
//{
//    NSMutableArray* arr = [@[@(2), @(3), @(1), @(5)] mutableCopy];
//    int sol = [PermMissingElement solution: arr];
//    XCTAssertEqual(4, sol);
//}
//
//-(void)testPermMissingElement1
//{
//    NSMutableArray* arr = [@[@(2), @(3), @(1), @(5), @(10), @(7), @(4), @(9), @(6)] mutableCopy];
//    int sol = [PermMissingElement solution: arr];
//    XCTAssertEqual(8, sol);
//}
//
//-(void)testPermMissingElement2
//{
//    NSMutableArray* arr = [@[@(2), @(3), @(1), @(5), @(10), @(7), @(4), @(9), @(6), @(8)] mutableCopy];
//    int sol = [PermMissingElement solution: arr];
//    XCTAssertEqual(11, sol);
//}
//
//-(void)testPermMissingElement3
//{
//    NSMutableArray* arr = [@[@(2), @(3), @(5), @(10), @(7), @(4), @(9), @(6), @(8), @(11)] mutableCopy];
//    int sol = [PermMissingElement solution: arr];
//    XCTAssertEqual(1, sol);
//}
//
//-(void)testTapeEquilibrium
//{
//    NSMutableArray* arr = [@[@(3), @(1), @(2), @(4), @(3)] mutableCopy];
//    int sol = [TapeEquilibrium solution: arr];
//    XCTAssertEqual(1, sol);
//}
//
//-(void)testTapeEquilibrium1
//{
//    NSMutableArray* arr = [@[@(400), @(300), @(600), @(100), @(500), @(200)] mutableCopy];
//    int sol = [TapeEquilibrium solution: arr];
//    XCTAssertEqual(500, sol);
//}
//
//-(void)testTapeEquilibrium2
//{
//    NSMutableArray* arr = [@[@(47), @(80), @(64), @(8), @(50), @(33)] mutableCopy];
//    int sol = [TapeEquilibrium solution: arr];
//    XCTAssertEqual(28, sol);
//}
//
//-(void)testTapeEquilibrium3
//{
//    NSMutableArray* arr = [@[@(5), @(10)] mutableCopy];
//    int sol = [TapeEquilibrium solution: arr];
//    XCTAssertEqual(5, sol);
//}
//
//-(void)testTapeEquilibrium4
//{
//    NSMutableArray* arr = [@[@(-3), @(-1), @(-2), @(-4), @(-3)] mutableCopy];
//    int sol = [TapeEquilibrium solution: arr];
//    XCTAssertEqual(1, sol);
//}
//
//-(void)testTapeEquilibrium5
//{
//    int count = 100000;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    
//    for (int i = 0; i < count; i++) {
//        if((i & 1) > 0)
//        {
//            [arr addObject:@(-1)];
//        }
//        else
//        {
//            [arr addObject:@(1)];
//        }
//    }
//    
//    int sol = [TapeEquilibrium solution: arr];
//    XCTAssertEqual(0, sol);
//}

@end
