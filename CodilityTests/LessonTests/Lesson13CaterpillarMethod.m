//
//  Lesson13CaterpillarMethod.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/8/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "CountDistinctSlices.h"

@interface Lesson13CaterpillarMethod : XCTestCase

@end

@implementation Lesson13CaterpillarMethod

//- (void)testCountDistinctSlices
//{
//    NSMutableArray* arr = [@[@(3), @(4), @(5), @(5), @(2)] mutableCopy];
//    int M = 6;
//    int sol = [CountDistinctSlices solutionX:M A:arr];
//    XCTAssertEqual(9, sol);
//}
//
//- (void)testCountDistinctSlices1
//{
//    NSMutableArray* arr = [@[@(8), @(4), @(6), @(5), @(0), @(1), @(3), @(7), @(9), @(10)] mutableCopy];
//    int M = 10;
//    int sol = [CountDistinctSlices solutionX:M A:arr];
//    XCTAssertEqual(55, sol);
//}
//
//- (void)testCountDistinctSlices2
//{
//    NSMutableArray* arr = [@[@(8), @(4), @(6)] mutableCopy];
//    int M = 10;
//    int sol = [CountDistinctSlices solutionX:M A:arr];
//    XCTAssertEqual(6, sol);
//}
//
//- (void)testCountDistinctSlices3
//{
//    NSMutableArray* arr = [@[@(0)] mutableCopy];
//    int M = 0;
//    int sol = [CountDistinctSlices solutionX:M A:arr];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testCountDistinctSlices4
//{
//    int count = 100000;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    int M = 100000;
//    
//    for (int i = 0; i < count; i++)
//    {
//        [arr addObject:@(i)];
//    }
//    
//    int sol = [CountDistinctSlices solutionX:M A:arr];
//    XCTAssertEqual(705082704, sol);
//}
@end
