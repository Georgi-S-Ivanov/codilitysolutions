//
//  Lesson6Leader.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/7/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "EquiLeader.h"
#import "Dominator.h"

@interface Lesson6Leader : XCTestCase

@end

@implementation Lesson6Leader

//- (void)testLeader
//{
//    NSMutableArray* arr = [@[@(4), @(3), @(4), @(4), @(4), @(2)] mutableCopy];
//    int sol = [EquiLeader solution:arr];
//    XCTAssertEqual(2, sol);
//}
//
//- (void)testLeader1
//{
//    NSMutableArray* arr = [@[@(1), @(3), @(2), @(1), @(3), @(2)] mutableCopy];
//    int sol = [EquiLeader solution:arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testLeader2
//{
//    NSMutableArray* arr = [@[@(-1000000000)] mutableCopy];
//    int sol = [EquiLeader solution:arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testLeader3
//{
//    int size = 100000;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:size];
//    
//    NSNumber* value = @(100000);
//    for (int i = 0; i < size; i++) {
//        [arr addObject:value];
//    }
//    
//    int sol = [EquiLeader solution:arr];
//    XCTAssertEqual(size - 1, sol);
//}
//
//- (void)testDominator
//{
//    NSMutableArray* arr = [@[@(3), @(4), @(3), @(2), @(3), @(-1), @(3), @(3)] mutableCopy];
//    int sol = [Dominator solution:arr];
//    XCTAssertEqual(7, sol);
//}
//
//- (void)testDominator1
//{
//    NSMutableArray* arr = [@[] mutableCopy];
//    int sol = [Dominator solution:arr];
//    XCTAssertEqual(-1, sol);
//}

@end
