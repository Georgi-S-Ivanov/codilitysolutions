//
//  Lesson4Sorting.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/4/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "MaxProductOfThree.h"
#import "Distinct.h"
#import "Triangle.h"
#import "NumberOfDiscIntersections.h"

@interface Lesson4Sorting : XCTestCase

@end

@implementation Lesson4Sorting

//- (void)testMaxProductOfThree
//{
//    NSMutableArray* arr = [@[@(-3), @(1), @(2), @(-2), @(5), @(6)] mutableCopy];
//    int sol = [MaxProductOfThree solution:arr];
//    XCTAssertEqual(60, sol);
//}
//
//- (void)testMaxProductOfThree1
//{
//    NSMutableArray* arr = [@[@(-3), @(-1), @(-2), @(-2), @(-5), @(-6)] mutableCopy];
//    int sol = [MaxProductOfThree solution:arr];
//    XCTAssertEqual(-4, sol);
//}
//
//- (void)testMaxProductOfThree2
//{
//    NSMutableArray* arr = [@[@(-3), @(1), @(-2), @(-2), @(-5), @(-6)] mutableCopy];
//    int sol = [MaxProductOfThree solution:arr];
//    XCTAssertEqual(4, sol);
//}
//
//- (void)testMaxProductOfThree3
//{
//    NSMutableArray* arr = [@[@(1000), @(900), @(901), @(950), @(999), @(989)] mutableCopy];
//    int sol = [MaxProductOfThree solution:arr];
//    XCTAssertEqual(988011000, sol);
//}
//
//- (void)testMaxProductOfThree4
//{
//    NSMutableArray* arr = [@[@(10), @(15), @(9), @(20)] mutableCopy];
//    int sol = [MaxProductOfThree solution:arr];
//    XCTAssertEqual(3000, sol);
//}
//
//- (void)testMaxProductOfThree5
//{
//    NSMutableArray* arr = [@[@(10), @(5), @(-100), @(-10), @(2), @(9)] mutableCopy];
//    int sol = [MaxProductOfThree solution:arr];
//    XCTAssertEqual(2000, sol);
//}
//
//- (void)testDistinct
//{
//    NSMutableArray* arr = [@[@(2), @(1), @(1), @(2), @(3), @(1)] mutableCopy];
//    int sol = [Distinct solution:arr];
//    XCTAssertEqual(3, sol);
//}
//
//- (void)testDistinct1
//{
//    NSInteger count = 1000000, half = count / 2;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    
//    for (NSInteger i = half; i > -half; i--)
//    {
//        [arr addObject:@(i)];
//    }
//    
//    int sol = [Distinct solution:arr];
//    XCTAssertEqual(count, sol);
//}
//
//- (void)testTriangle
//{
//    NSMutableArray* arr = [@[@(10), @(2), @(5), @(1), @(8), @(20)] mutableCopy];
//    int sol = [Triangle solution:arr];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testTriangle1
//{
//    NSMutableArray* arr = [@[@(10), @(50), @(5), @(1)] mutableCopy];
//    int sol = [Triangle solution:arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testTriangle2
//{
//    NSMutableArray* arr = [@[@(-9),  @(-7),  @(-6),  @(-4),  @(1),  @(-1),   @(0),   @(8),  @(-10),  @(10),   @(7), @(-5),   @(6),  @(-2),   @(5)] mutableCopy];
//    int sol = [Triangle solution:arr];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testTriangle3
//{
//    NSMutableArray* arr = [@[] mutableCopy];
//    int sol = [Triangle solution:arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testTriangle4
//{
//    int count = 100000, half = count / 2;
//    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:count];
//    
//    for (int i = half - 1; i >= -half; i--)
//    {
//        [arr addObject:@(i)];
//    }
//    
//    int sol = [Triangle solution:arr];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testNumberOfDiscIntersections
//{
//    NSMutableArray* arr = [@[@(1), @(5), @(2), @(1), @(4), @(0)] mutableCopy];
//    int sol = [NumberOfDiscIntersections solution:arr];
//    XCTAssertEqual(11, sol);
//}
//
//- (void)testNumberOfDiscIntersections1
//{
//    NSMutableArray* arr = [@[@(1), @(5)] mutableCopy];
//    int sol = [NumberOfDiscIntersections solution:arr];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testNumberOfDiscIntersections2
//{
//    NSMutableArray* arr = [@[@(1), @(5), @(0)] mutableCopy];
//    int sol = [NumberOfDiscIntersections solution:arr];
//    XCTAssertEqual(2, sol);
//}
//
//- (void)testNumberOfDiscIntersections3
//{
//    NSMutableArray* arr = [@[@(1), @(1), @(1)] mutableCopy];
//    int sol = [NumberOfDiscIntersections solution:arr];
//    XCTAssertEqual(3, sol);
//}
//
//- (void)testNumberOfDiscIntersections4
//{
//    NSMutableArray* arr = [@[@(1), @(1), @(1)] mutableCopy];
//    int sol = [NumberOfDiscIntersections solution:arr];
//    XCTAssertEqual(3, sol);
//}
//
//- (void)testNumberOfDiscIntersections5
//{
//    NSMutableArray* arr = [@[@(1), @(1), @(2), @(1)] mutableCopy];
//    int sol = [NumberOfDiscIntersections solution:arr];
//    XCTAssertEqual(5, sol);
//}

@end
