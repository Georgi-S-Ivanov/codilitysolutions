//
//  Lesson7MaximumSlice.m
//  CodilityObjC
//
//  Created by Georgi Ivanov on 6/7/15.
//  Copyright (c) 2015 GeorgiIvanov. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "MaxProfit.h"
#import "MaxSliceSum.h"

@interface Lesson7MaximumSlice : XCTestCase

@end

@implementation Lesson7MaximumSlice

//- (void)testMaxProfit
//{
//    NSMutableArray* arr = [@[@(23171), @(21011), @(21123), @(21366), @(21013), @(21367)] mutableCopy];
//    int sol = [MaxProfit solution:arr];
//    XCTAssertEqual(356, sol);
//}
//
//- (void)testMaxProfit1
//{
//    NSMutableArray* arr = [@[@(21000)] mutableCopy];
//    int sol = [MaxProfit solution:arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testMaxProfit2
//{
//    NSMutableArray* arr = [@[] mutableCopy];
//    int sol = [MaxProfit solution:arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testMaxProfit3
//{
//    NSMutableArray* arr = [@[@(5), @(1), @(2), @(2), @(3), @(4), @(5), @(4), @(3)] mutableCopy];
//    int sol = [MaxProfit solution:arr];
//    XCTAssertEqual(4, sol);
//}
//
//- (void)testMaxProfit4
//{
//    NSMutableArray* arr = [@[@(5), @(4), @(3), @(2), @(1), @(0), @(1)] mutableCopy];
//    int sol = [MaxProfit solution:arr];
//    XCTAssertEqual(1, sol);
//}
//
//- (void)testMaxProfit5
//{
//    NSMutableArray* arr = [@[@(5), @(4), @(3), @(2), @(1), @(0)] mutableCopy];
//    int sol = [MaxProfit solution:arr];
//    XCTAssertEqual(0, sol);
//}
//
//- (void)testMaxSliceSum
//{
//    NSMutableArray* arr = [@[@(3), @(2), @(-6), @(4), @(0)] mutableCopy];
//    int sol = [MaxSliceSum solution:arr];
//    XCTAssertEqual(5, sol);
//}
//
//- (void)testMaxSliceSum1
//{
//    NSMutableArray* arr = [@[@(1), @(2), @(3), @(4), @(5)] mutableCopy];
//    int sol = [MaxSliceSum solution:arr];
//    XCTAssertEqual(15, sol);
//}
//
//- (void)testMaxSliceSum2
//{
//    NSMutableArray* arr = [@[@(-1000000)] mutableCopy];
//    int sol = [MaxSliceSum solution:arr];
//    XCTAssertEqual(-1000000, sol);
//}
//
//- (void)testMaxSliceSum3
//{
//    NSMutableArray* arr = [@[@(-1), @(2), @(-3), @(4), @(-5)] mutableCopy];
//    int sol = [MaxSliceSum solution:arr];
//    XCTAssertEqual(4, sol);
//}

@end
